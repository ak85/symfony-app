<?php

declare(strict_types=1);

namespace App\Tests\MessageHandler\HistoricalQuote;

use App\Message\HistoricalQuote\NotifyUser;
use App\MessageHandler\HistoricalQuote\NotifyUserHandler;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Swift_Mailer;

final class NotifyUserHandlerTest extends TestCase
{
    /**
     * @dataProvider validData
     */
    public function test(
        string $fromAddress,
        string $toAddress,
        string $companyName,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ) {
        $mailer = $this->createMock(Swift_Mailer::class);

        $message = new NotifyUser(
            $toAddress,
            $companyName,
            $startDate,
            $endDate
        );

        $mailer
            ->expects($this->once())
            ->method('send');

        $sut = new NotifyUserHandler($mailer, $fromAddress);
        $sut($message);
    }

    public function validData(): array
    {
        return [
            'test1' => [
                'fromAddress' => 'fromAddress@example.com',
                'toAddress' => 'toAddress@example.com',
                'companyName' => 'companyName',
                'startDate' => new DateTimeImmutable(),
                'endDate' => new DateTimeImmutable(),
            ],
        ];
    }
}
