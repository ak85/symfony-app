<?php

declare(strict_types=1);

namespace App\Tests\Entity;

use App\Entity\HistoricalQuote;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;

final class HistoricalQuoteTest extends TestCase
{
    /**
     * @dataProvider validData
     */
    public function test(
        DateTimeInterface $date,
        float $openValue,
        float $highValue,
        float $lowValue,
        float $closeValue
    ) {
        $sut = new HistoricalQuote($date, $openValue, $highValue, $lowValue, $closeValue);
        $this->assertSame($date, $sut->getDate());
        $this->assertSame($openValue, $sut->getOpenValue());
        $this->assertSame($highValue, $sut->getHighValue());
        $this->assertSame($lowValue, $sut->getLowValue());
        $this->assertSame($closeValue, $sut->getCloseValue());
    }

    public function validData(): array
    {
        return [
            'test1' => [
                'date' => new DateTimeImmutable(),
                'openValue' => 80.35,
                'highValue' => 64.10,
                'lowValue' => 15.28,
                'closeValue' => 59.19,
            ],
        ];
    }
}
