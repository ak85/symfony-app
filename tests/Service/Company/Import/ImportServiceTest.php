<?php

declare(strict_types=1);

namespace App\Tests\Service\Company\Import;

use App\Service\Company\GetCompanies\GetCompaniesServiceInterface;
use App\Service\Company\Import\ImportService;
use Doctrine\ORM\EntityManagerInterface;
use PHPUnit\Framework\TestCase;

class ImportServiceTest extends TestCase
{
    public function testValidData()
    {
        $expectedJson = $this->validJson();

        $getCompaniesService = $this->createMock(GetCompaniesServiceInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $getCompaniesService
            ->expects($this->once())
            ->method('get')
            ->willReturn($expectedJson);

        $entityManager
            ->expects($this->exactly(2))
            ->method('persist');

        $sut = new ImportService($getCompaniesService, $entityManager);
        $this->assertTrue($sut->import());
    }

    public function testInvalidData()
    {
        $expectedJson = 'invalidJson';

        $getCompaniesService = $this->createMock(GetCompaniesServiceInterface::class);
        $entityManager = $this->createMock(EntityManagerInterface::class);

        $getCompaniesService
            ->expects($this->once())
            ->method('get')
            ->willReturn($expectedJson);

        $entityManager
            ->expects($this->never())
            ->method('persist');

        $sut = new ImportService($getCompaniesService, $entityManager);
        $this->assertFalse($sut->import());
    }

    private function validJson(): string
    {
        return <<<JSON
[
  {
    "Company Name": "Company Name 1", 
    "Financial Status": "N",
    "Market Category": "G", 
    "Round Lot Size": 100.0, 
    "Security Name": "iShares MSCI All Country Asia Information Technology Index Fund", 
    "Symbol": "AAIT", 
    "Test Issue": "N"
  },
  {
    "Company Name": "Company Name 2", 
    "Financial Status": "N", "Market Category": "Q", 
    "Round Lot Size": 100.0, 
    "Security Name": "American Airlines Group, Inc. - Common Stock", 
    "Symbol": "AAL", 
    "Test Issue": "N"
  }
]
JSON;
    }
}
