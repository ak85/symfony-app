<?php

declare(strict_types=1);

namespace App\Tests\Service\Company\GetCompanies;

use App\Service\Company\GetCompanies\GetCompaniesService;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

class GetCompaniesServiceTest extends TestCase
{
    public function test()
    {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);
        $mockUri = 'testUri';
        $mockMethod = 'testMethod';
        $expectedResult = 'expected result';

        $mockHttpClient
            ->expects($this->once())
            ->method('request')
            ->with($mockMethod, $mockUri)
            ->willReturn($mockResponse);

        $mockResponse
            ->expects($this->once())
            ->method('getContent')
            ->with(false)
            ->willReturn($expectedResult);

        $sut = new GetCompaniesService($mockHttpClient, $mockUri, $mockMethod);
        $this->assertSame($expectedResult, $sut->get());
    }
}
