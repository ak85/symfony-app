<?php

declare(strict_types=1);

namespace App\Tests\Service\HistoricalQuote\GetHistoricalQuotes;

use App\Service\HistoricalQuote\FetchData\FetchDataServiceInterface;
use App\Service\HistoricalQuote\GetHistoricalQuotes\GetHistoricalQuotesService;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

final class GetHistoricalQuotesServiceTest extends TestCase
{
    /**
     * @dataProvider validData
     */
    public function test(
        string $companySymbol,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate,
        string $csv,
        array $data
    ) {
        $fetchDataService = $this->createMock(FetchDataServiceInterface::class);
        $serializer = $this->createMock(DecoderInterface::class);

        $fetchDataService
            ->expects($this->once())
            ->method('fetch')
            ->with($companySymbol, $startDate, $endDate)
            ->willReturn($csv);

        $serializer
            ->expects($this->once())
            ->method('decode')
            ->with($csv, 'csv')
            ->willReturn($data);

        $sut = new GetHistoricalQuotesService($fetchDataService, $serializer);
        $results = $sut->get($companySymbol, $startDate, $endDate);
        $this->assertSame(count($results), count($results));
    }

    public function validData()
    {
        return [
            'test1' => [
                'symbol' => 'symbol',
                'startDate' => DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-01'),
                'endDate' => DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-10'),
                'csv' => $this->validCsv(),
                'data' => $this->validQuoteData(),
            ],
        ];
    }

    private function validCsv(): string
    {
        return <<<CSV
Date,Open,High,Low,Close,Volume,Ex-Dividend,Split Ratio,Adj. Open,Adj. High,Adj. Low,Adj. Close,Adj. Volume
2003-01-02,14.36,14.92,14.35,14.8,3239800.0,0.0,1.0,0.92273007233099,0.95871397487315,0.92208750264274,0.95100313861411,45357200.0
2003-01-03,14.8,14.93,14.59,14.9,2633100.0,0.0,1.0,0.95100313861411,0.9593565445614,0.9375091751608,0.95742883549664,36863400.0
CSV;
    }

    private function validQuoteData(): array
    {
        return [
            [
                'Date' => '2003-01-02',
                'Open' => '14.36',
                'High' => '14.92',
                'Low' => '14.35',
                'Close' => '14.8',
            ],
            [
                'Date' => '2003-01-03',
                'Open' => '14.8',
                'High' => '14.93',
                'Low' => '14.59',
                'Close' => '14.9',
            ],
        ];
    }
}
