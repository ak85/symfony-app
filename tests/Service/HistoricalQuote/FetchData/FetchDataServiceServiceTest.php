<?php

declare(strict_types=1);

namespace App\Tests\Service\HistoricalQuote\FetchData;

use App\Service\HistoricalQuote\FetchData\FetchDataServiceService;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Symfony\Contracts\HttpClient\ResponseInterface;

final class FetchDataServiceServiceTest extends TestCase
{
    /**
     * @dataProvider validData
     */
    public function test(
        string $responseBody,
        int $responseStatus,
        string $symbol,
        string $uri,
        string $method,
        string $order,
        string $stringStartDate,
        string $stringEndDate,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ) {
        $mockHttpClient = $this->createMock(HttpClientInterface::class);
        $mockResponse = $this->createMock(ResponseInterface::class);
        $options = [
            'query' => [
                'order' => $order,
                'start_date' => $stringStartDate,
                'end_date' => $stringEndDate,
            ],
        ];

        $mockHttpClient
            ->expects($this->once())
            ->method('request')
            ->with($method, $uri, $options)
            ->willReturn($mockResponse);

        $mockResponse
            ->expects($this->once())
            ->method('getStatusCode')
            ->willReturn($responseStatus);

        $mockResponse
            ->expects($this->once())
            ->method('getContent')
            ->with(false)
            ->willReturn($responseBody);

        $sut = new FetchDataServiceService($mockHttpClient);
        $this->assertSame($responseBody, $sut->fetch($symbol, $startDate, $endDate));
    }

    public function validData(): array
    {
        return [
            'test1' => [
                'responseBody' => 'responseBody',
                'responseStatus' => 200,
                'symbol' => 'symbol',
                'uri' => 'https://www.quandl.com/api/v3/datasets/WIKI/symbol.csv',
                'method' => 'GET',
                'order' => 'asc',
                'stringStartDate' => '2019-01-01',
                'stringEndDate' => '2019-01-10',
                'startDate' => DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-01'),
                'endDate' => DateTimeImmutable::createFromFormat('Y-m-d', '2019-01-10'),
            ],
        ];
    }
}
