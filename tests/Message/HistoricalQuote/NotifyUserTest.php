<?php

declare(strict_types=1);

namespace App\Tests\Message\HistoricalQuote;

use App\Message\HistoricalQuote\NotifyUser;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;

class NotifyUserTest extends TestCase
{
    /**
     * @dataProvider validData
     */
    public function test(
        string $email,
        string $companyName,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ) {
        $sut = new NotifyUser($email, $companyName, $startDate, $endDate);
        $this->assertSame($email, $sut->getEmail());
        $this->assertSame($companyName, $sut->getCompanyName());
        $this->assertSame($startDate, $sut->getStartDate());
        $this->assertSame($endDate, $sut->getEndDate());
    }

    public function validData(): array
    {
        return [
            'test1' => [
                'email' => 'email',
                'companyName' => 'companyName',
                'startDate' => new DateTimeImmutable(),
                'endDate' => new DateTimeImmutable(),
            ],
        ];
    }
}
