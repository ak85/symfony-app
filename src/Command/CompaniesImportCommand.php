<?php

declare(strict_types=1);

namespace App\Command;

use App\Service\Company\Import\ImportServiceInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CompaniesImportCommand extends Command
{
    protected static $defaultName = 'app:companies-import';
    private ImportServiceInterface $importService;

    public function __construct(ImportServiceInterface $importService)
    {
        $this->importService = $importService;
        parent::__construct();
    }

    protected function configure()
    {
        $this->setDescription('Import companies from web service');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        $success = $this->importService->import();
        $this->ioResult($success, $io);

        return 0;
    }

    private function ioResult(bool $success, SymfonyStyle $io): void
    {
        if ($success) {
            $io->success('Companies were imported');

            return;
        }

        $io->warning('No companies were imported');
    }
}
