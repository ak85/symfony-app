<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeInterface;

final class HistoricalQuote
{
    private DateTimeInterface $date;
    private float $openValue;
    private float $highValue;
    private float $lowValue;
    private float $closeValue;

    public function __construct(
        DateTimeInterface $date,
        float $openValue,
        float $highValue,
        float $lowValue,
        float $closeValue
    ) {
        $this->date = $date;
        $this->openValue = $openValue;
        $this->highValue = $highValue;
        $this->lowValue = $lowValue;
        $this->closeValue = $closeValue;
    }

    public function getDate(): DateTimeInterface
    {
        return $this->date;
    }

    public function getOpenValue(): float
    {
        return $this->openValue;
    }

    public function getHighValue(): float
    {
        return $this->highValue;
    }

    public function getLowValue(): float
    {
        return $this->lowValue;
    }

    public function getCloseValue(): float
    {
        return $this->closeValue;
    }
}
