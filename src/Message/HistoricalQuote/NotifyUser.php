<?php

declare(strict_types=1);

namespace App\Message\HistoricalQuote;

use DateTimeInterface;

final class NotifyUser
{
    private string $email;
    private string $companyName;
    private DateTimeInterface $startDate;
    private DateTimeInterface $endDate;

    public function __construct(
        string $email,
        string $companyName,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ) {
        $this->email = $email;
        $this->companyName = $companyName;
        $this->startDate = $startDate;
        $this->endDate = $endDate;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function getCompanyName(): string
    {
        return $this->companyName;
    }

    public function getStartDate(): DateTimeInterface
    {
        return $this->startDate;
    }

    public function getEndDate(): DateTimeInterface
    {
        return $this->endDate;
    }
}
