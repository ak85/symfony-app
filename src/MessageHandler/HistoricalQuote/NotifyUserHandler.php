<?php

declare(strict_types=1);

namespace App\MessageHandler\HistoricalQuote;

use App\Message\HistoricalQuote\NotifyUser;
use Swift_Mailer;
use Swift_Message;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

final class NotifyUserHandler implements MessageHandlerInterface
{
    private Swift_Mailer $mailer;
    private string $fromAddress;

    public function __construct(Swift_Mailer $mailer, string $fromAddress)
    {
        $this->mailer = $mailer;
        $this->fromAddress = $fromAddress;
    }

    public function __invoke(NotifyUser $message)
    {
        $message = (new Swift_Message())
            ->setFrom($this->fromAddress)
            ->setTo($message->getEmail())
            ->setSubject($message->getCompanyName())
            ->setBody($this->getBody($message))
        ;

        $this->mailer->send($message);
    }

    private function getBody(NotifyUser $message): string
    {
        return <<<BODY
From {$message->getStartDate()->format('Y-m-d')} to {$message->getEndDate()->format('Y-m-d')}
BODY;
    }
}
