<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\Company;
use App\Form\HistoricalQuotesType;
use App\Message\HistoricalQuote\NotifyUser;
use App\Service\HistoricalQuote\GetHistoricalQuotes\GetHistoricalQuotesServiceInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class HistoricalQuotesController extends AbstractController
{
    private GetHistoricalQuotesServiceInterface $historicalQuotesService;
    private MessageBusInterface $messageBus;

    public function __construct(
        GetHistoricalQuotesServiceInterface $historicalQuotesService,
        MessageBusInterface $messageBus
    ) {
        $this->historicalQuotesService = $historicalQuotesService;
        $this->messageBus = $messageBus;
    }

    /**
     * @Route("/historical/quotes", name="historical_quotes")
     */
    public function index(Request $request)
    {
        $form = $this->createForm(HistoricalQuotesType::class);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            /** @var Company $company */
            $company = $data['company'];
            $startDate = $data['startDate'];
            $endDate = $data['endDate'];
            $email = $data['email'];

            $historicalQuotes = $this->historicalQuotesService->get(
                $company->getSymbol(),
                $startDate,
                $endDate
            );

            if (empty($historicalQuotes)) {
                return $this->render(
                    'historical_quotes/index.html.twig',
                    [
                        'form' => $form->createView(),
                        'errorMessage' => 'No results, please try again',
                    ]
                );
            }

            $this->messageBus->dispatch(
                new NotifyUser(
                    $email,
                    $company->getName(),
                    $startDate,
                    $endDate
                )
            );

            return $this->render(
                'historical_quotes/success.html.twig',
                [
                    'company' => $company,
                    'startDate' => $startDate,
                    'endDate' => $endDate,
                    'historicalQuotes' => $historicalQuotes,
                ]
            );
        }

        return $this->render(
            'historical_quotes/index.html.twig',
            [
                'form' => $form->createView(),
                'errorMessage' => '',
            ]
        );
    }
}
