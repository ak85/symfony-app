<?php

declare(strict_types=1);

namespace App\Service\Company\GetCompanies;

interface GetCompaniesServiceInterface
{
    public function get(): string;
}
