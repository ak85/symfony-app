<?php

declare(strict_types=1);

namespace App\Service\Company\GetCompanies;

use Symfony\Contracts\HttpClient\HttpClientInterface;

final class GetCompaniesService implements GetCompaniesServiceInterface
{
    private HttpClientInterface $httpClient;
    private string $uri;
    private string $method;

    public function __construct(
        HttpClientInterface $httpClient,
        string $uri,
        string $method
    ) {
        $this->httpClient = $httpClient;
        $this->uri = $uri;
        $this->method = $method;
    }

    public function get(): string
    {
        $response = $this->httpClient->request($this->method, $this->uri);

        return $response->getContent(false);
    }
}
