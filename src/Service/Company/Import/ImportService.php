<?php

declare(strict_types=1);

namespace App\Service\Company\Import;

use App\Entity\Company;
use App\Service\Company\GetCompanies\GetCompaniesServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

final class ImportService implements ImportServiceInterface
{
    private GetCompaniesServiceInterface $getCompaniesService;
    private EntityManagerInterface $entityManager;

    public function __construct(
        GetCompaniesServiceInterface $getCompaniesService,
        EntityManagerInterface $entityManager
    ) {
        $this->getCompaniesService = $getCompaniesService;
        $this->entityManager = $entityManager;
    }

    public function import(): bool
    {
        $jsonCompanies = $this->getCompaniesService->get();
        $arrayCompanies = json_decode($jsonCompanies, true);
        if (empty($arrayCompanies)) {
            return false;
        }

        array_walk(
            $arrayCompanies,
            function (array $arrayCompany) {
                $company = new Company();
                $company = $company->setName($arrayCompany['Company Name']);
                $company = $company->setSymbol($arrayCompany['Symbol']);
                $this->entityManager->persist($company);
            }
        );

        $this->entityManager->flush();

        return true;
    }
}
