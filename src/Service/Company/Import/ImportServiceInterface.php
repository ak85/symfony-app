<?php

declare(strict_types=1);

namespace App\Service\Company\Import;

interface ImportServiceInterface
{
    public function import(): bool;
}
