<?php

declare(strict_types=1);

namespace App\Service\HistoricalQuote\FetchData;

use DateTimeInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

final class FetchDataServiceService implements FetchDataServiceInterface
{
    private const BASE_URI = 'https://www.quandl.com/api/v3/datasets/WIKI/';
    private HttpClientInterface $httpClient;

    public function __construct(HttpClientInterface $httpClient)
    {
        $this->httpClient = $httpClient;
    }

    public function fetch(
        string $companySymbol,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ): string {
        $uri = self::BASE_URI.$companySymbol.'.csv';
        $response = $this->httpClient->request(
            'GET',
            $uri,
            [
                'query' => [
                    'order' => 'asc',
                    'start_date' => $startDate->format('Y-m-d'),
                    'end_date' => $endDate->format('Y-m-d'),
                ],
            ]
        );

        if ($response->getStatusCode() !== 200) {
            return '';
        }

        return $response->getContent(false);
    }
}
