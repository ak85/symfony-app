<?php

declare(strict_types=1);

namespace App\Service\HistoricalQuote\FetchData;

use DateTimeInterface;

interface FetchDataServiceInterface
{
    public function fetch(
        string $companySymbol,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ): string;
}
