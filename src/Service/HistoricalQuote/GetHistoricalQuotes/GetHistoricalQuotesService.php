<?php

declare(strict_types=1);

namespace App\Service\HistoricalQuote\GetHistoricalQuotes;

use App\Entity\HistoricalQuote;
use App\Service\HistoricalQuote\FetchData\FetchDataServiceInterface;
use DateTimeImmutable;
use DateTimeInterface;
use Symfony\Component\Serializer\Encoder\DecoderInterface;

final class GetHistoricalQuotesService implements GetHistoricalQuotesServiceInterface
{
    private FetchDataServiceInterface $fetchDataService;
    private DecoderInterface $serializer;

    public function __construct(
        FetchDataServiceInterface $fetchDataService,
        DecoderInterface $serializer
    ) {
        $this->fetchDataService = $fetchDataService;
        $this->serializer = $serializer;
    }

    /**
     * @return HistoricalQuote[]
     */
    public function get(
        string $companySymbol,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ): array {
        $csv = $this->fetchDataService->fetch($companySymbol, $startDate, $endDate);
        $data = $this->serializer->decode($csv, 'csv');

        return array_map(
            function (array $rawData) {
                return new HistoricalQuote(
                    DateTimeImmutable::createFromFormat('Y-m-d', $rawData['Date']),
                    (float) $rawData['Open'],
                    (float) $rawData['High'],
                    (float) $rawData['Low'],
                    (float) $rawData['Close'],
                );
            },
            $data
        );
    }
}
