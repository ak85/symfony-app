<?php

declare(strict_types=1);

namespace App\Service\HistoricalQuote\GetHistoricalQuotes;

use App\Entity\HistoricalQuote;
use DateTimeInterface;

interface GetHistoricalQuotesServiceInterface
{
    /**
     * @return HistoricalQuote[]
     */
    public function get(
        string $companySymbol,
        DateTimeInterface $startDate,
        DateTimeInterface $endDate
    ): array;
}
