<?php

declare(strict_types=1);

namespace App\Form;

use DateTime;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\Validator\Context\ExecutionContextInterface;

class HistoricalQuotesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'company',
                EntityType::class,
                [
                    'class' => 'App\Entity\Company',
                    'choice_label' => 'getFullName',
                    'placeholder' => 'Choose an company',
                ]
            )
            ->add(
                'startDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                ]
            )
            ->add(
                'endDate',
                DateType::class,
                [
                    'widget' => 'single_text',
                    'constraints' => [
                        new Constraints\Callback(function ($endDate, ExecutionContextInterface $context) {
                            $startDate = $context->getRoot()->getData()['startDate'];

                            if (is_a($endDate, DateTime::class) && is_a($startDate, DateTime::class)) {
                                if ($endDate->format('Ymd') - $startDate->format('Ymd') <= 0) {
                                    $context
                                        ->buildViolation('End date should be after start date')
                                        ->addViolation();
                                }
                            }
                        }),
                    ],
                ]
            )
            ->add(
                'email',
                EmailType::class
            )
            ->add(
                'submit',
                SubmitType::class
            )
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([]);
    }
}
